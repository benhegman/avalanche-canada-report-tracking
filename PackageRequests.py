import pip
import requests
import json
import os
import csv
import pandas as pd
from datetime import date

os.chdir(r"C:\Users\bhegman.stu\Documents\AvyProj")

# function to get only the avalanche MIN reports
def filter_by_obtype(x):
    return any([x for x in x.get("obs") if x.get("obtype") == "avalanche"])

# returns all the MIN reports
avyCan = requests.get(
    "https://www.avalanche.ca/api/min/submissions?client=web&last=5000%3Adays"
).json()
coordinates = list(map(lambda x: x.get("latlng"), filter(filter_by_obtype, avyCan)))  # coordinates for just the avys

# creating a list of dicts of just the avalanches
avys = []
for dictx in avyCan:
    observations = dictx.get("obs")
    for i in observations:
        if i.get("obtype") == "avalanche":
            avys.append(dictx)

# create empty dict for all the avy attributes to be appended to
d = {}

# pulling all the attributes for to be used as columns. avys[0:1] is being used as template for its attributes
attribList = []
for dictx in avys[0:1]:
    observations = dictx.get("obs")
    for i in observations:
        if i.get("obtype") == "avalanche":
            ob = i.get("ob")
            for thing in ob:
                if type(ob.get(thing)) != type(ob):
                    attribList.append(thing)

# place the attributes as columns in the dictionary
for i in attribList:
    d[i] = []

# add all attributes for all avalanches
for item in attribList:
    for dictx in avys:
        observations = dictx.get("obs")
        for i in observations:
            if i.get("obtype") == "avalanche":
                ob = i.get("ob")
                d[item].append(ob.get(item))

# convert them to a pandas df
df = pd.DataFrame(d)

# creating latitude and longitude columns
lati = []
longi = []
for event in avys:
    coord = event.get("latlng")
    lati.append(coord[0])
    longi.append(coord[1])
# Assigning Lat and Long
df["Latitude"] = lati
df["Longitude"] = longi

# renaming columns and cleaning up the table
dfUpdate1 = df.rename(
    columns={
        "windExposure": "Wind Exposure",
        "runoutZoneElevation": "Run Out Zone Elevation",
        "avalancheSize": "Avalanche Size",
        "avalancheObsComment": "Avalanche Comments",
        "slabWidth": "Slab Width",
        "crustNearWeakLayer": "Crust Near Weak Layer?",
        "runLength": "Run Length",
        "avalancheNumber": "Number of Avalanches In This Report",
        "avalancheOccurrenceTime": "Avalanche Time",
        "avalancheObservation": "Estimated Occurence Time",
        "weakLayerBurialDate": "Weak Layer Burial Date",
        "startZoneElevation": "Start Zone Elevation",
    }
)
# more renaming
dfNew = dfUpdate1.rename(
    columns={
        "slabThickness": "Slab Thickness",
        "startZoneIncline": "Start Zone Incline",
        "triggerSubtype": "Trigger Subtype",
        "triggerDistance": "Trigger Distance",
        "triggerType": "Trigger Type",
        "avalancheOccurrenceEpoch": "Avalanche Date",
        "vegetationCover": "Vegetation Cover",
    }
)
# drop the temporary lat lng column - we already have the permanent lat lng
dfNew = dfNew.drop(columns="tempLatlng")
# fill NAs with a dash
dfNew = dfNew.fillna("-")
# todays date for file creation
today = date.today()
todayFormatted = today.strftime("%b%d_%Y")
# writing to a csv
export_csv = dfNew.to_csv(
    r"C:\Users\bhegman.stu\Documents\AvyProj\Avalanches" + todayFormatted + ".csv", encoding="utf-8"
)
