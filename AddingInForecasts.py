import requests
import os
import arcpy
from datetime import datetime, timedelta

os.chdir(r"C:\Users\bhegman.stu\Documents\AvyProj")
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\AvyProj\AvyProj\AvyProj.gdb"
arcpy.env.overwriteOutput = True

# gets just the files for the region shapefiles
Regions = [x for x in arcpy.ListFeatureClasses() if not x.startswith(("A", "a"))]

# Creates a file with avalanche points contained in that region for all regions
for i in Regions:
    arcpy.MakeFeatureLayer_management("AvalanchesJan9_2020_XYTableToPoint", "test_lyr")
    arcpy.management.SelectLayerByLocation(
        "test_lyr", "INTERSECT", i, None, "NEW_SELECTION", "NOT_INVERT"
    )
    arcpy.CopyFeatures_management("test_lyr", "avysContainedIn" + i)

# corresponding API endings for each region from Avy Canada website
RegionAPIDict = {
    "avysContainedInCariboos": "cariboos",
    "avysContainedInSouthCoast": "south-coast",
    "avysContainedInKananaskisCountryAlbertaParks": "kananaskis",
    "avysContainedInKootenayBoundary": "kootenay-boundary",
    "avysContainedInLizardRangeAndFlathead": "lizard-range",
    "avysContainedInNorthColumbia": "north-columbia",
    "avysContainedInNorthRockies": "north-rockies",
    "avysContainedInNorthwestCoastal": "northwest-coastal",
    "avysContainedInNorthwestInland": "northwest-inland",
    "avysContainedInPurcells": "purcells",
    "avysContainedInSeaToSky": "sea-to-sky",
    "avysContainedInSouthCoastInland": "south-coast-inland",
    "avysContainedInSouthColumbia": "south-columbia",
    "avysContainedInSouthRockies": "south-rockies",
    "avysContainedInYukon": "yukon",
}

# puts all the files created earlier in a list
RegionFileList = [
    x for x in arcpy.ListFeatureClasses() if x.startswith("avysContained")
]
# add fields to the attribute tables
for region in RegionFileList:
    arcpy.AddFields_management(
        region,
        [
            ["DayOfForecast", "TEXT"],
            ["DayBeforeForecast", "TEXT"],
            ["TwoDayBeforeForecast", "TEXT"],
        ],
    )

# adding in the forecasts - this is just for sea to sky region right now
for region in RegionAPIDict.keys():
    with arcpy.da.UpdateCursor(
        region,
        [
            "Avalanche_Date",
            "DayOfForecast",
            "DayBeforeForecast",
            "TwoDayBeforeForecast",
        ],
    ) as cursor:
        for row in cursor:  # save the date of each avalanche from the attribute table
            date = row[0]
            if date is None:  # skip if there is no date from the MIN report
                continue
            dayOf = datetime.strftime(
                date, "%Y-%m-%d"
            )  # put it in the correct date format
            oneDayBefore = datetime.strftime(date - timedelta(days=1), "%Y-%m-%d")
            twoDayBefore = datetime.strftime(date - timedelta(days=2), "%Y-%m-%d")
            try:
                # API requests for the day of, day before, and two days before
                dayOfAPI = requests.get(
                    "https://www.avalanche.ca/api/bulletin-archive/"
                    + dayOf
                    + "T08:00:00.000Z/"
                    + RegionAPIDict[region]
                    + ".json"
                ).json()
                DangerRatings = dayOfAPI.get("dangerRatings")
                if type(DangerRatings) is list:
                    # sets the column "DayOfForecast" in the attribute table with the alp/tln/btl ratings
                    row[1] = (
                        DangerRatings[0].get("dangerRating").get("alp")
                        + DangerRatings[0].get("dangerRating").get("tln")
                        + DangerRatings[0].get("dangerRating").get("btl")
                    )
                else:
                    continue
            except ValueError:
                print("ValueError")
            try:
                oneDayAPI = requests.get(
                    "https://www.avalanche.ca/api/bulletin-archive/"
                    + oneDayBefore
                    + "T08:00:00.000Z/"
                    + RegionAPIDict[region]
                    + ".json"
                ).json()
                DangerRatings = oneDayAPI.get("dangerRatings")
                if type(DangerRatings) is list:
                    # sets the column "DayBeforeForecast" in the attribute table with the alp/tln/btl ratings
                    row[2] = (
                        DangerRatings[0].get("dangerRating").get("alp")
                        + DangerRatings[0].get("dangerRating").get("tln")
                        + DangerRatings[0].get("dangerRating").get("btl")
                    )
                else:
                    continue
            except ValueError:
                print("ValueError")
            try:
                twoDayAPI = requests.get(
                    "https://www.avalanche.ca/api/bulletin-archive/"
                    + twoDayBefore
                    + "T08:00:00.000Z/"
                    + RegionAPIDict[region]
                    + ".json"
                ).json()
                DangerRatings = twoDayAPI.get("dangerRatings")
                if type(DangerRatings) is list:
                    # sets the column "TwoDayBeforeForecast" in the attribute table with the alp/tln/btl ratings
                    row[3] = (
                        DangerRatings[0].get("dangerRating").get("alp")
                        + DangerRatings[0].get("dangerRating").get("tln")
                        + DangerRatings[0].get("dangerRating").get("btl")
                    )
                else:
                    continue
            except ValueError:
                print("ValueError")
                # update the attribute table
            cursor.updateRow(row)
    del row, cursor
    print(region + ": Done")
