import requests
import os
import arcpy

os.chdir(r"C:\Users\bhegman.stu\Documents\AvyProj")
arcpy.env.workspace = r"C:\Users\bhegman.stu\Documents\AvyProj\AvyProj\AvyProj.gdb"
arcpy.env.overwriteOutput = True

# creating the shapefiles for each region from coordinates
RegionCoordinates = requests.get("https://www.avalanche.ca/api/forecasts").json()

outSR = arcpy.SpatialReference(
    "WGS 1984"
)  # output spatial reference - this should match the spatial reference of the avalanche reports.  Double check this
for Region in RegionCoordinates:
    RegionDict = RegionCoordinates.get("features")
    singlePartRegions = [x for x in RegionDict if not x.get("id") == "south-coast"]  # south coast is multipart polygon so is handled differently
    del singlePartRegions[-1]  # gets rid of the bad duplicate of the yukon the API gives
    for AvyRegion in singlePartRegions:
        Coordinates = AvyRegion["geometry"]["coordinates"]
        RegionName = AvyRegion["properties"]["name"]
        RegionName = RegionName.replace(",", "")
        for i in Coordinates[0]:  # get rid of the weird nested list format
            Coordinates.append(i)
        Coordinates.pop(0)
        CoordinatesClean = [x for x in Coordinates if isinstance(x, list)]  # gets rid of rouge random float coords
        array = arcpy.Array()
        for coordinate in CoordinatesClean:
            array.add(arcpy.Point(coordinate[0], coordinate[1]))
        feature = arcpy.Polygon(array, outSR)
        arcpy.CopyFeatures_management(feature, RegionName.replace(" ", ""))


# south coast is formatted differently so we need to deal with it separately
RegionList = RegionCoordinates.get("features")
SouthCoastFeatures = RegionList[6].get("geometry")
SouthCoastCoords = SouthCoastFeatures.get("coordinates")
RegionName = "SouthCoast"
SouthCoastPart1 = SouthCoastCoords[0]
SouthCoastPart2 = [x for x in SouthCoastCoords[1:]]
partList = [SouthCoastPart1, SouthCoastPart2]
for part in partList:
    CoordinatesClean = [x for x in part if isinstance(x, list)]
    array = arcpy.Array()
    for coord in CoordinatesClean:
        array.add(arcpy.Point(coord[0], coord[1]))
    feature = arcpy.Polygon(array, outSR)
    arcpy.CopyFeatures_management(
        feature, RegionName.replace(" ", "") + str(partList.index(part))
    )
